import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent {
  enteredValue = ''
  newPost = 'NO CONTENT';

  // onAddPost(postInput: HTMLTextAreaElement) {
  //   console.dir(postInput);
  //   this.newPost = postInput.value;
  // }

  onAddPost() {
    this.newPost = this.enteredValue;
  }

}
